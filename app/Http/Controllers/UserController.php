<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index',[
            'users' => User::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validData = $request->validate([
            'name'=>'required|min:3|',
            'last_name'=>'required|min:3',
            'document'=>'required|min:3|numeric',//|unique:connection.users,document',
            'email'=>'required|min:3|email',//|unique:connection.users,email',
            'phone'=>'required|min:3|numeric',
        ]);   
        
        $newUser = new User();
        $newUser->name = $validData['name'];
        $newUser->last_name = $validData['last_name'];
        $newUser->document = $validData['document'];
        $newUser->email = $validData['email'];
        $newUser->phone = $validData['phone'];

        $newUser->save();

        return redirect('/users')->with('success', "El usuario $newUser->name se creo satisfactoriamente");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::findOrFail($id);
        return view('user.edit',[
            'user'=>$user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        
        $validData = $request->validate([
            'name'=>'required|min:3|',
            'last_name'=>'required|min:3',
            'document'=>'required|min:3|numeric',//|unique:connection.users,document',
            'email'=>'required|min:3|email',//|unique:connection.users,email',
            'phone'=>'required|min:3|numeric',
        ]);   

        $user->name = $validData['name'];
        $user->last_name = $validData['last_name'];
        $user->document = $validData['document'];
        $user->email = $validData['email'];
        $user->phone = $validData['phone'];

        $user->update();

        return redirect('/users')->with('success', "El usuario $user->name fue editado");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
